import requests
import json

# Set your ServiceNow credentials
username = "admin"
password = "fA0F^MA7+kax"

# ServiceNow instance URL
instance_url = "https://dev62725.service-now.com"

headers = {"Content-Type":"application/json","Accept":"application/json"}



# URL for retrieving incidents (adjust the table name if needed)
incidents_url = f"{instance_url}/api/now/table/incident"

# Define query parameters
query_params = {
    "sysparm_query": "short_descriptionLIKEAzure SQL Server reachable from untrusted^state!=2^state!=7",
    #"sysparm_fields": "number,short_description,priority,state",
}

# Make the API request
response = requests.get(incidents_url, auth=(username, password), headers=headers, params=query_params )

# Check if the request was successful
if response.status_code == 200:
    incidents = response.json().get("result", [])
    if incidents:
        print("Incidents matching the criteria:")
        for incident in incidents:
            number = incident.get("number")
            short_description = incident.get("short_description")
            priority = incident.get("priority")
            state = incident.get("state")
            print(f"Number: {number}, Short Description: {short_description}, Priority: {priority}, State: {state}")
            # Add a comment to the incident
            comment_url = f"{incidents_url}/{incident.get('sys_id')}"
            comment_payload = {"comments": "Hi\nAzure SQL server exposed on the internet is a very critical security issue that could lead to brute force or DDoS attacks.\nPLease take this incident as a priority."}
            comment_response = requests.patch(comment_url, auth=(username, password), json=comment_payload)
            if comment_response.status_code == 200:
                print(f"Comment added to incident {number}.")
            else:
                print(f"Error adding comment to incident {number}. Status code: {comment_response.status_code}")
    else:
        print("No incidents found.")
else:
    print(f"Error fetching incidents. Status code: {response.status_code}")
